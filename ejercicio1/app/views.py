from flask import Blueprint, request, jsonify

bp = Blueprint('main', __name__)

@bp.route('/', methods=['GET'])
def home():
    return "Bienvenido a mi API!"

@bp.route('/set/<key>/<value>', methods=['POST'])
def set_value(key, value):
    from flask import current_app as app
    app.redis.set(key, value)
    return jsonify(success=True)

@bp.route('/get/<key>', methods=['GET'])
def get_value(key):
    from flask import current_app as app
    value = app.redis.get(key)
    if value is None:
        return jsonify(success=False)
    else:
        return jsonify(success=True, value=value.decode('utf-8'))
