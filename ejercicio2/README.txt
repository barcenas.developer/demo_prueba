Crear un perfil de usuario con un POST a /profile con un JSON como este: {"user_id": "1", "name": "John", "description": "Me gusta el cine"}
Obtener un perfil de usuario con un GET a /profile/1
Dar "me gusta" a otro usuario con un POST a /like con un JSON como este: {"from_user_id": "1", "to_user_id": "2"}
Obtener los "me gusta" de un usuario con un GET a /likes/1



ejemplo:
curl -X POST -H "Content-Type: application/json" -d '{"user_id": "1", "name": "John", "description": "Me gusta el cine"}' http://127.0.0.1:5000/profile
curl http://127.0.0.1:5000/profile/1
curl -X POST -H "Content-Type: application/json" -d '{"from_user_id": "1", "to_user_id": "2"}' http://127.0.0.1:5000/like
curl http://127.0.0.1:5000/likes/1
