from flask import Blueprint, request, jsonify

bp = Blueprint('main', __name__)

@bp.route('/profile/<user_id>', methods=['GET'])
def get_profile(user_id):
    from flask import current_app as app
    profile = app.redis.hgetall(f'profile:{user_id}')
    if not profile:
        return jsonify(success=False)
    else:
        # Redis devuelve los datos en bytes, así que los convertimos a string
        profile = {k.decode('utf-8'): v.decode('utf-8') for k, v in profile.items()}
        return jsonify(success=True, profile=profile)

@bp.route('/profile', methods=['POST'])
def set_profile():
    from flask import current_app as app
    user_id = request.json['user_id']
    name = request.json['name']
    description = request.json['description']
    app.redis.hmset(f'profile:{user_id}', {'name': name, 'description': description})
    return jsonify(success=True)

@bp.route('/like', methods=['POST'])
def add_like():
    from flask import current_app as app
    from_user_id = request.json['from_user_id']
    to_user_id = request.json['to_user_id']
    app.redis.lpush(f'likes:{from_user_id}', to_user_id)
    return jsonify(success=True)

@bp.route('/likes/<user_id>', methods=['GET'])
def get_likes(user_id):
    from flask import current_app as app
    likes = app.redis.lrange(f'likes:{user_id}', 0, -1)
    # Redis devuelve los datos en bytes, así que los convertimos a string
    likes = [like.decode('utf-8') for like in likes]
    return jsonify(success=True, likes=likes)
